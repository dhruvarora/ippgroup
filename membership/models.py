from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

now = timezone.now()

class Plan(models.Model):
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)
    description = RichTextField(blank=True, null=True)
    price = models.FloatField(default=0.00)

    def __str__(self):
        return self.name


class Membership(models.Model):
    date_added = models.DateField(default=now)
    user = models.ForeignKey(User)
    plan = models.ForeignKey(Plan)
    expiry_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.plan.name + " for " + self.user.first_name

class Payment(models.Model):
    date_added = models.DateField(default=now)
    user = models.ForeignKey(User)
    membership = models.ForeignKey(Membership)
    txn_id = models.CharField(max_length=150)
    payment_method = models.CharField(max_length=150, default="Prepaid CCAV")
