from django.contrib import admin

from .models import Plan, Membership, Payment

class PaymentInline(admin.TabularInline):
    model = Payment

class MembershipAdmin(admin.ModelAdmin):
    list_display = ["date_added", "user", "plan", "expiry_date"]
    list_filter = ["plan"]
    search_fields = ["user__first_name", "user__last_name"]
    inlines = [PaymentInline]

admin.site.register(Plan)
admin.site.register(Membership, MembershipAdmin)
admin.site.register(Payment)
