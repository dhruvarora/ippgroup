from django.shortcuts import render

from django.contrib.sites.models import Site
from website.models import SiteProfile
from .models import Article, Category

# Function to get site object
def getSite(id):
    return Site.objects.get(pk=id)

# Method to get category list on the basis of the site id supplied
def categoryList(site_id):
    return Category.objects.filter(sites=getSite(site_id))

# Index view for article app
def index(request, site_id, slug):
    site = SiteProfile.objects.get(site=getSite(site_id))
    recent_posts = Article.objects.filter(sites=site.site, published=True).order_by("-date_added")[:5]
    popular_posts = Article.objects.filter(sites=site.site, published=True).order_by("hits")[:5]
    featured = Article.objects.filter(sites=site.site, published=True, featured=True).order_by("-date_added")[:3]


    return render(request, "articles/index.html", {
    "site":site, "categories":categoryList(site.site.pk), "featured":featured, "recent":recent_posts, "popular":popular_posts,
    })

# Category view for the articles app
def categoryView(request, site_id, slug, cat_id, cat_slug):
    site = getSite(site_id)
    category = Category.objects.get(pk=cat_id)

    return render("articles/category.html", {
    "category":category, "categories":categoryList(site.site.pk)
    })
