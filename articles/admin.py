from django.contrib import admin

from .models import Article, Comment, Category

class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "published", "date_added", "author", "hits")
    list_display_links = ["title"]
    readonly_fields = ["hits"]
    list_editable = ["published"]
    list_filter = ["categories", "published", "author"]
    search_fields = ["author", "title"]

admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment)
admin.site.register(Category)
