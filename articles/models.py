from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField
from django.contrib.sites.models import Site

now = timezone.now()

YESNO_CHOICES = (
    (True, "Yes"),
    (False, "No")
)

class Category(models.Model):
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)
    sites = models.ManyToManyField(Site)

    def __str__(self):
        return self.name

class Article(models.Model):
    date_added = models.DateField(default=now)
    published = models.BooleanField(default=True, choices=YESNO_CHOICES)
    categories = models.ManyToManyField(Category)
    featured = models.BooleanField(default=False, choices=(
        (True, "Yes"),
        (False, "No")
    ))
    title = models.CharField(max_length=150)
    author = models.CharField(max_length=50)
    introText = models.TextField(blank=True, null=True)
    content = RichTextField(blank=True, null=True)
    introImage = models.FileField(max_length=150, upload_to="articles/intro-images", blank=True, null=True)
    mainImage = models.FileField(max_length=150, upload_to="articles/main-images", blank=True, null=True)
    metaDescription = models.TextField(blank=True, null=True)
    metaKeywords = models.CharField(max_length=250, blank=True, null=True)
    hits = models.IntegerField(default=0)
    sites = models.ManyToManyField(Site)

    def __str__(self):
        return self.title

class Comment(models.Model):
    article = models.ForeignKey(Article)
    date_added = models.DateField(default=now)
    published = models.BooleanField(default=True, choices=YESNO_CHOICES)
    authorName = models.CharField(max_length=150)
    authorEmail = models.EmailField(max_length=150)
    comment = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.article.title + " comment by " + self.authorName
