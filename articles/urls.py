from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^categories/(?P<cat_id>\d+)/(?P<cat_slug>.+)/$', views.categoryView, name="category"),
]
