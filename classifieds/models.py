from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField
from django.utils import timezone
from django.contrib.auth.models import User

now = timezone.now()

class Category(models.Model):
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)
    published = models.BooleanField(default=True, choices=(
        (True, "Yes"),
        (False, "No"),
    ))

    def __str__(self):
        return self.name

class Post(models.Model):
    date_added = models.DateField(default=now)
    user = models.ForeignKey(User)
    categories = models.ManyToManyField(Category)
    title = models.CharField(max_length=150)
    image = models.FileField(upload_to="classifieds/", max_length=150)
    intro_text = models.TextField(blank=True, null=True)
    about = RichTextField(blank=True, null=True)
    featured = models.BooleanField(default=False, choices=(
        (True, "Yes"),
        (False, "No"),
    ))
    published = models.BooleanField(default=False, choices=(
        (True, "Yes"),
        (False, "No"),
    ))
    tags = models.CharField(max_length=150, blank=True, null=True, help_text="Enter tags for SEO Meta")
    price = models.CharField(max_length=75, default="Contact for Quote")
    hits = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Query(models.Model):
    date_added = models.DateField(default=now)
    post = models.ForeignKey(Post)
    customer_name = models.CharField(max_length=150)
    customer_email = models.EmailField(max_length=150)
    customer_mobile = models.BigIntegerField(blank=True, null=True)
    messaged = models.TextField(blank=True, null=True)
