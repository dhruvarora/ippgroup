from django.contrib import admin

from .models import Post, Query, Category

class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "date_added", "published"]
    search_fields = ["name"]
    list_editable = ["published"]
    list_display_links = ["name"]

class PostAdmin(admin.ModelAdmin):
    list_display = ["title", "date_added", "user", "featured", "published", "price", "hits"]
    list_filter = ["featured", "published"]
    search_fields = ["title", "user__first_name", "user__last_name", "price"]
    list_editable = ["featured", "published"]
    list_display_links = ["title"]
    readonly_fields = ["hits"]

class QueryAdmin(admin.ModelAdmin):
    list_display = ["post__title", "date_added", "customer_name", "customer_email", "customer_mobile"]
    search_fields = ["post__title", "customer_name", "customer_email", "customer_mobile"]

admin.site.register(Post, PostAdmin)
admin.site.register(Query)
admin.site.register(Category, CategoryAdmin)
