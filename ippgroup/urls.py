from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'', include("website.urls", namespace="website")),
    url(r'^admin/', admin.site.urls),
    url(r'^newsletter/', include('newsletter.urls', namespace="newsletters")),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
