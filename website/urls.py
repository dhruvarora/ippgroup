from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<site_id>\d+)/(?P<slug>.+)/', include("articles.urls", namespace="articles")),
    url(r'^login/$', views.login_view, name="login"),
    url(r'^logout/$', views.logout_view, name="logout"),
    url(r'^signup/$', views.signup_view, name="signup"),
]
