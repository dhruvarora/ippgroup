from __future__ import unicode_literals

from django.db import models
from django.contrib.sites.models import Site
from ckeditor.fields import RichTextField

class SiteProfile(models.Model):
    site = models.OneToOneField(Site)
    image = models.FileField(max_length=150, upload_to="sites-images/")
    logo = models.FileField(max_length=150, upload_to="sites-logos/", default="logo.png")
    about = RichTextField(blank=True, null=True)
    template = models.CharField(max_length=150, default="index.html")

    def __str__(self):
        return self.site.domain
