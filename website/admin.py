from django.contrib import admin

from django.contrib.sites.models import Site
from .models import SiteProfile

class SiteProfileStackedInline(admin.StackedInline):
    model = SiteProfile

class SiteAdmin(admin.ModelAdmin):
    inlines = [SiteProfileStackedInline]

admin.site.unregister(Site)
admin.site.register(Site, SiteAdmin)
