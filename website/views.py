from django.shortcuts import render

from django.contrib.sites.models import Site
from .models import SiteProfile
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


def index(request):
    sites = SiteProfile.objects.all()

    return render(request, "index.html", {
    "sites":sites,
    })

def login_view(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponse("success")
        else:
            return HttpResponse("blocked")
    else:
        return HttpResponse("failed")

def logout_view(request):
    logout(request)
    return redirect("website:index")

def signup_view(request):
    username = request.POST.get("username")
    email = request.POST.get("email")
    password = request.POST.get("password")
    user = User.objects.create_user(username=username, email=email, password=password)
    try:
        user = User.objects.create_user(username=username, email=email, password=password)
    except:
        return HttpResponse("failed")
    else:
        user.first_name = request.POST.get("fname")
        user.last_name = request.POST.get("name")
        user.save()
        return HttpResponse("success")
