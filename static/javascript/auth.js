// Login Ajax

$("#login-submit").on('click', function(){
  var username = $("#email").val();
  var password = $("#password").val();
  var csrf = $("#csrf").val();
  var datastring = "username=" + username + "&password=" + password + "&csrfmiddlewaretoken=" + csrf;
  $.ajax({
    url: '/login/',
    type: 'POST',
    data: datastring,
    success:function(response){
      if (response=="success"){
        alert("Login succesfull");
        window.location = window.location.href;
      }
      else if(response=="failed"){
        alert("Login failed");
        window.location = window.location.href;
      }
    }
  });
})

// Signup Ajax
$("#signup-submit").on('click', function(){
  var username = $("#email2").val();
  var email = $("#email2").val();
  var csrf = $("#csrf2").val();
  var password = $("#password2").val();
  var fname = $("#fname").val();
  var lname = $("#lname").val();
  var datastring = "username=" + username + "&email=email" + email + "&password=" + password + "&csrfmiddlewaretoken=" + csrf + "&fname=" + fname + "&lname=" + lname;
  $.ajax({
    url: '/signup/',
    type: 'POST',
    data: datastring,
    success:function(response){
      if(response=="success"){
        alert("Signed Up. Please login to access website");
        window.location = "/";
      }
      else{
        alert("Email already exists, please use forgot password or different email");
        window.location = "/";
      }
    }
  });
})
